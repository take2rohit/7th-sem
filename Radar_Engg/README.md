# Radar Communication

## PPT topics

| Lecture          | Topics                                            |
| ---------------- | ------------------------------------------------- |
| Radar Lecture 1  |                                                   |
| Radar Lecture 2  | Unambigous range                                  |
| Radar Lecture 3  | Radar Equation                                    |
| Radar Lecture 4  | Formulas, radar detection                         |
| Radar Lecture 5  | Noise Figure                                      |
| Radar Lecture 6  | Probability of false Alarm, Albersheim Formula    |
| Radar Lecture 7  | Integration of radar pulses                       |
| Radar Lecture 8  | Radar crossection                                 |
| Radar Lecture 9  | Final rdr Range eqn, rdr Losses, Surveillance rdr |
| Radar Lecture 10 | IMP FORMULAS                                      |
| Radar Lecture 11 | Doppler freq shift                                |
| Radar Lecture 12 | CW doppler radar pulse radar                      |
| Radar Lecture 13 | MTI radar stalo and coho, blind speed             |
| Radar Lecture 14 | Blind speed, clutter attn, delay canceller        |
| Radar Lecture 15 | MTI imporvement factor, Staggered prf             |
| Radar Lecture 16 | Imp formula, example, assignment solution         |
| Radar Lecture 17 | Blind Phases and Digital MTI Processing           |
| Radar Lecture 18 | MTI Moving platform                               |


## Other info
Videos on Google Classroom


Reference Book: Introduction to Radar System– M.Skalrit

## Structure

1.	Introduction

2.	Lecture-2 (1-22)
    1.	what is Radar?
    2.	Radar Range
    3.	Maximum Unambiguous Range
    4.	Radar Waveform (Pulse Radar)

3.	Lecture-3 (23-43)
    1.	CLASSIFICATION OF RADAR SYSTEMS
    2.	Continuous Wave (CW) Doppler Radars
    3.	Pulsed Radar
    4.	RADAR RANGE EQUATION
    5.	FACTORS AFFECTING RADAR RANGE

4.	Lecture-4 (44-68)
    1.	Radar Block Diagram
        1.	Transmitter
        2.	Antenna
        3.	Beam width of antenna
        4.	Duplexer
        5.	Receiver
        6.	Matched Filter
        7.	Second Detector or Demodulator
        8.	Video Amplifier
        9.	Decision Making
        10.	Integration
        11.	Signal Processor
    2.	Radar Display
        1.	A-scope
        2.	B-scope
        3. Plan Position Indicator (PPI)
    3.	Effective Antenna Aperture

5.	Lecture-5 (69-89)
    1.	LIMITATIONS OF SIMPLE RADAR EQUATION
    2.	THRESHOLD DETECTION
    3.	TARGET DETECTION IN PRESENCE OF NOISE
    4.	SELECTION OF THRESHOLD
    5.	SOURCES OF NOISE RECEIVED BY RADAR
    6.	THERMAL NOISE
    7.	NOISE FIGURE
    8.	MINIMUM DETECTABLE SIGNAL
        1.	RADAR RANGE EQUATION
    8.	ENVELOPE DETECTOR
    9.	PROBABILITY OF FALSE ALARM

6.	Lecture-6 (90-110)
    1.	false alarm time, 𝑻𝒇𝒂
    2.	PROBABILITY OF DETECTION
    3.	ALBERSHEIM FORMULA

7.	Lecture-7 (111-131)
    1.	INTEGRATION OF PULSES
    2.	COHERENT INTEGRATION
    3.	NON COHERENT INTEGRATION
    4.	RADAR RANGE EQUATION
    5.	ALBERSHEIM FORMULA

8.	Lecture-8 (132 - 143)
    1.	RADAR CROSS SECTION
    2.	RADAR CROSS SECTION OF A SIMPLE SPHERE
        1.	Rayleigh region
        2.	Optical region
        3. Mie or Resonance region

9.	Lecture-9  (144-162)
    1.	RADAR CROSS SECTION
    2.	THREAT’S VIEW OF RADAR
    3.	RADAR RANGE EQUATION USING
        1.	AVERAGE TRANSMITTER POWER 
        2.	SYSTEM LOSSES
    4.	ATMOSPHERIC LOSSES
    5.	FINAL RADAR EQUATION
    6.	SURVEILLANCE RADAR
    7.	DIFFERENCE BETWEEN TRACK RADAR AND SURVEILLANCE RADAR
    8.	SURVEILLANCE RADAR
    1.	SURVEILLANCE RADAR RANGE EQUATION

10.	Lecture-10 (FORMULA BOOK)

11.	Lecture-11 (163-179)
    1.	RADAR CLUTTER
    2.	TYPES OF CLUTTER
        1.	Surface Clutter
        2.	Volume clutter
        3.	Point Clutters
    3.	CLUTTER PROBLEM
    4.	HANDLING THE CLUTTER
    5.	DOPPLER EFFECT
        1.	Moving Target Indicator (MTI)
        2.	Pulse Doppler (PD)
    6.	DOPPLER FREQUENCY SHIFT

12.	Lecture-12 (180-197)
    1.	Doppler Shift
    2.	SIMPLE CONTINUOUS WAVE DOPPLER RADAR
    3.	FILTER CHARACTERISTICS
    4.	PULSE RADAR THAT EXTRACTS DOPPLER FREQUENCY SHIFT OF ECHO SIGNAL FROM MOVING TARGET
    5.	Pulse Radar
    6.	Principle Of Operation
    7.	ECHO PULSE TRAIN
    8.	SUCCESSIVE SWEEP SUBTRACTION OF MTI RADAR A SCOPE DISPLAY
        1.	SINGLE DELAY LINE CANCELLER

13.	Lecture-13 (198-213)
    1.	MTI RADAR WITH POWER AMPLIFIER TRANSMITTER
    2.	RECEIVER EXCITER PORTION OF MTI  RADAR
    3.	SINGLE DELAY LINE CANCELER FREQUENCY RESPONSE OF SINGLE DELAY LINE CANCELER
    4.	BLIND SPEEDS

14.	Lecture-14 (214-229)
    1.	CLUTTER ATTENUATION
    2.	MTI IMPROVEMENT FACTOR
    3.	DOUBLE DELAY LINE CANCELER AND THREE PULSE DELAY LINE CANCELER
    4.	N PULSE DELAY LINE CANCELER

15.	Lecture-15 (215-247)
    1.	MTI IMPROVEMENT FACTOR (Formula)
    2.	TRANSVERSAL (NON RECURSIVE) FILTER
    3.	STAGGERED PULSE REPETITION FREQUENCIES
    4.	FREQUENCY RESPONSE OF SINGLE DELAY LINE CANCELER WITH PRF RATIO=5:4
    5.	FIRST BLIND SPEED WITH STAGGERED PRF

16.	Lecture-16 (Questions + Formulas)

17.	Lecture-17 (248-259)
    1.	MTI doppler filters
    2.	DIGITAL MTI PROCESSING ADVANTAGES
    3.	BLIND PHASES
    4.	DIGITAL MTI SIGNAL PROCESSOR
    1.	LIMITATIONS

18.	Lecture-18 (260-274)
    1.	AMTI AIRBORNE MTI
    2.	HOW TO REDUCE CLUTTER DOPPLER SHIFT
    3.	TACCAR AND DPCA
    4.	PROBLEMS WITH MTI
    5.	DOPPLER AMBIGUITIES
    6.	RANGE AMBIGUITIES
    7.	HIGH PRF PULSE DOPPLER
    8.	MTI Vs PULSE DOPPLER

19.	Lecture-19 (275-292)
    1.	SOLUTION TO RANGE AMBIGUITIES
    2.	PULSE DOPPLER RADAR
    3.	HIGH PRF PULSE DOPPLER RADAR
    4.	SPECTRUM OF TRANSMITTED SIGNAL
    5.	SPECTRUM OF RECEIVED SIGNAL
    6.	ECLIPSING LOSS
    7.	MEDIUM PRF PULSE DOPPLER RADAR
    8.	COMPARISON b/w High, Medium and Low PRF’s

20.	Lecture-20 (293-305)
    1.	CW RADAR
    2.	FREQUENCY MODULATED CW RADAR
    3.	APPLICATION OF FM CW RADAR
